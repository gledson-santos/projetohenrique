from unittest import TestCase

from conversorMedidas import ConversoresMassa, ConversoresAltura, CalculoImc


class Test_Conversor_Peso(TestCase):

    '''
    EXEMPLOS DE TESTES PARA SEREM IMPLEMENTADOS

    * VERIFICAR SE O CALCULO ESTÁ CORRETOR
    * VERIFICAR SE O CALCULO ESTÁ CORRETOR COM UM NÚMERO FLOAT
    * VERIFICAR SE O RETORNO É VAZIO - É OBRIGATÓRIO TER VALOR CADASTRADO
    *

    um conversor de libras pra quilos
    um conversor de polegadas pra metros
    e um conversor de pés pra polegadas
    '''
    def setUp(self):
        self.conversor_massa = ConversoresMassa()

    '''
        TESTE FUNÇÃO CONVERSÃO DE LIBRAS
    '''
    def test_libras_valor_inteiro(self):
        #Testa se a função está retornando o valor correto
        self.assertEqual(self.conversor_massa.converter_libra_para_quilo(1), 0.4536)

    def test_libras_valor_float(self):
        # Testa o valor para calculo sendo FLOAT
        self.assertEqual(self.conversor_massa.converter_libra_para_quilo(2.55), 1.15668)




class Test_Conversor_Altura(TestCase):

    '''
    TESTE CONVERSÃO POLEGADAS
    '''
    def setUp(self):
        self.conversor_altura = ConversoresAltura()


    def test_polegadas_valor_inteiro(self):
        # Testa se a função está retornando o valor correto
        self.assertEqual(self.conversor_altura.converter_polegadas_para_metros(1), 0.0254)

    def test_polegadas_valor_float(self):
        # Testa o valor para calculo sendo FLOAT
        self.assertEqual(self.conversor_altura.converter_polegadas_para_metros(2.55), 0.06477)

    '''
    TESTE CONVERSÃO PÉS
    '''

    def test_pes_valor_inteiro(self):
        # Testa se a função está retornando o valor correto
        self.assertEqual(self.conversor_altura.converter_pes_para_polegada(1), 12)

    def test_pes_valor_float(self):
        # Testa o valor para calculo sendo FLOAT
        self.assertEqual(self.conversor_altura.converter_pes_para_polegada(2.55), 30.599999999999998)

    '''
        TESTE CONVERSÃO PÉS e POLEGADAS para METROS
    '''

    def test_pes_polegadas_em_metros_valor_inteiro(self):
        # Testa se a função está retornando o valor correto
        self.assertEqual(self.conversor_altura.converter_pes_e_polegadas_em_metros(1, 4), 0.4064)

    def test_pes_polegadas_em_metros_valor_float(self):
        # Testa o valor para calculo sendo FLOAT
        self.assertEqual(self.conversor_altura.converter_pes_e_polegadas_em_metros(1.55, 1), 0.49784)



class Test_Imc(TestCase):

    '''
    IMC = peso (quilos) ÷ altura² (metros)
    '''

    def setUp(self):
        self.calculo_imc = CalculoImc()

    def test_calculo_imc_valor_inteiro(self):
        self.assertEqual(self.calculo_imc.calculo_imc(0, 68.89764, 198.416), 29.388242233438184)

    def test_calculo_imc_valor_float(self):
        self.assertEqual(self.calculo_imc.calculo_imc(5.7414700, 0, 198.416),29.388242233438184)

