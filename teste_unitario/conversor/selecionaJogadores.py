from conversorMedidas import ConversoresAltura
from conversorMedidas import ConversoresMassa
from conversorMedidas import CalculoImc
import numpy as np


class Seleciona_Jogador(object):

    def verifica_jogador_alto(self, altura_pes):
        conversor = ConversoresAltura()

        if altura_pes >= 5.7 and altura_pes <= 6.5:
            return conversor.converter_pes_polegadas_em_metros(altura_pes,0)
        print("jogador fora do padrão de altura")
        return 'n'


    def verifica_peso_libras(self, peso):
        massa = ConversoresMassa()

        if peso in range(141, 209):
            return massa.converter_libra_para_quilo(peso)
        print("jogador fora do padrão de peso")
        return 'n'


    def converte_imc_jogador(self, altura, peso):
        altura_pes = self.verifica_jogador_alto(altura)
        peso_libras = self.verifica_peso_libras(peso)

        if altura_pes == 'n' or peso_libras=='n':
            return 'Não OK'
        imc = CalculoImc()
        resultado = imc.calculo_imc(altura_pes, 0,peso_libras)
        return round(resultado,2)