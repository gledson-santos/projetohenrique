import unittest

from selecionaJogadores import Seleciona_Jogador

class Test_Seleciona_Jogador(unittest.TestCase):

    jogador = Seleciona_Jogador()

    def test_testar_jogardor_alto(self):
        self.assertEqual(1.83, self.jogador.verifica_jogador_alto(6))

    def test_testar_jogador_baixo(self):
        self.assertEqual('n', self.jogador.verifica_jogador_alto(1))

    def test_verifica_peso_jogador(self):
        self.assertEqual(65.77, self.jogador.verifica_peso_libras(145))

    def test_verifica_peso_jogador_fora_padrao(self):
        self.assertEqual('n', self.jogador.verifica_peso_libras(90))

    def test_verifica_imc_jogador(self):
        self.assertEqual(98.41, self.jogador.converte_imc_jogador(6, 150))