from string import *
from unicodedata import normalize

class Exercicios(object):
    '''
    Exercício 2


    Crie uma aplicação para validação de CPF.
    Em um CPF, os dois últimos dígitos, são os dígitos verificadores,
    que são obtidos a partir de um calculo sobre os nove primeiros dígitos,
    a formula de geração dos dígitos verificadores é a seguinte:

    1. Calcular o primeiro digito verificador.

    Para calcular o primeiro dígito verificador,
    precisamos multiplicar sequencialmente os primeiros nove números do CPF
    por 10, 9, 8, 7, 6, 5, 4, 3 e 2 respectivamente, e somar o resultado, exemplo:

    CPF 3 3 3 1 2 4 9 8 7

    Multiplicador 10 9 8 7 6 5 4 3 2

    Resultado 30 27 24 7 12 20 36 24 14

    A soma total de cada resultado é 194.

    Agora, divida o resultado total por 11,
    se o resto da divisão for menor que 2,
    então o primeiro digito verificador é zero,
    caso contrário subtraia o total de 11 para obter o primeiro dígito.

    Neste exemplo o resto da divisão de 194 por 11 é 7,
    então precisamos subtrair 11 - 7 para obter 4 como o primeiro dígito verificador.
    ###################################################################################


    2. Calcular o segundo dígito verificador.

    Para calcular o segundo dígito verificador vamos fazer um calculo similar ao do primeiro dígito,
    só que agora temos o primeiro digito verificador,
    então iremos multiplicar os primeiros dez números do CPF por 11, 10, 9, 8, 7, 6, 5, 4, 3 e 2
    respectivamente, e somar o resultado, exemplo:

    CPF 3 3 3 1 2 4 9 8 7 4

    Multiplicador 11 10 9 8 7 6 5 4 3 2

    Resultado 33 30 27 8 14 24 45 32 21 8

    A soma total de cada resultado é 242.

    Agora, divida o resultado total por 11,
    se o resto da divisão for menor que 2,
    então o segundo digito verificador é zero,
    caso contrário subtraia o total de 11 para obter o segundo dígito.

    Neste exemplo o resto da divisão de 242 por 11 é 0, então usamos zero como segundo dígito verificador.

    Então, para este exemplo de CPF: 333.124.987, temos o resultado 40 como dígito verificador.

    '''

    msgErroCpf = 'CPF Inválido'
    msgOKCpf = 'CPF Válido'

    def limparCaracter(self, numero):

        numero = str(numero)
        n = numero.isdigit()

        if n == False:
            for c in punctuation:
                numero = numero.replace(c,'')

        n = numero.isdigit()

        a = len(numero)
        if n == False or a != 11:
            return self.msgErroCpf

        return numero


    def validaCpf(self, cpf):
        numero = self.limparCaracter(cpf)

        if numero == self.msgErroCpf:
            return self.msgErroCpf

        multiplicador1=[10, 9, 8, 7, 6, 5, 4, 3, 2]
        multiplicador2=[11, 10, 9, 8, 7, 6, 5, 4, 3, 2]

        x=0
        soma=0
        while x < 9:
            a = numero[x]
            a = int(a)
            soma += a * multiplicador1[x]
            x += 1

        x = 0
        soma2 = 0
        while x < 10:
            a = numero[x]
            a = int(a)
            soma2 += a * multiplicador2[x]
            x += 1

        resultado1 = soma%11
        resultado2 = soma2%11


        if resultado1 < 2:
            digito1 = resultado1
        else:
            digito1 = 11 - resultado1

        if resultado2 < 2:
            digito2 = resultado2
        else:
            digito2 = 11 - resultado2

        digito1 = str(digito1)
        digito2 = str(digito2)

        digNumero = numero[9:]
        digValidacao = digito1+digito2

        if digNumero == digValidacao:
            return self.msgOKCpf

        return self.msgErroCpf


    '''
    Exercício 1

    Crie uma aplicação para criptografar e descriptografar usando a Cifra de César.
    Esta é uma forma simples de criptografia, que consistem em mover as letras do alfabeto.
    Dado um número n, vamos mover as letras no alfabeto, por exemplo:

    Dado n = 3 e o texto “Rafael Guimaraes Sakurai”,
    vamos aumentar em 3 cada caractere, assim a versão criptografada ficará “Udidho Jzlpdudhv Vdnzudl”,
    e para descriptografar basta diminuir em 3 cada caractere.
    '''

    def cifraCesar(self, palavra):
        palavra = normalize('NFKD', palavra).encode('ASCII','ignore').decode('ASCII')
        alfabeto = list(ascii_lowercase)
        alfabetoMaiusculo = list(ascii_uppercase)
        listaCompletaAlfabeto = alfabeto+alfabeto+alfabetoMaiusculo+alfabetoMaiusculo
        novaPalavra=[]

        for c in palavra:
            if c in listaCompletaAlfabeto:
                novaPalavra.append(listaCompletaAlfabeto.index(c))
            else:
                novaPalavra.append(c)

        palavraCript=[]
        for n in novaPalavra:
            if type(n)== int:
                palavraCript.append(listaCompletaAlfabeto[n+3])
            else:
                palavraCript.append(n)

        palavraCript = ''.join(palavraCript)

        return palavraCript



