import unittest
from teste_unitario.exerciciosComplementares.exercicio import Exercicios

class TestExercicios(unittest.TestCase, Exercicios):
    '''
    Exercício 2


    Crie uma aplicação para validação de CPF.
    Em um CPF, os dois últimos dígitos, são os dígitos verificadores,
    que são obtidos a partir de um calculo sobre os nove primeiros dígitos,
    a formula de geração dos dígitos verificadores é a seguinte:

    1. Calcular o primeiro digito verificador.

    Para calcular o primeiro dígito verificador,
    precisamos multiplicar sequencialmente os primeiros nove números do CPF
    por 10, 9, 8, 7, 6, 5, 4, 3 e 2 respectivamente, e somar o resultado, exemplo:

    CPF 3 3 3 1 2 4 9 8 7

    Multiplicador 10 9 8 7 6 5 4 3 2

    Resultado 30 27 24 7 12 20 36 24 14

    A soma total de cada resultado é 194.

    Agora, divida o resultado total por 11,
    se o resto da divisão for menor que 2,
    então o primeiro digito verificador é zero,
    caso contrário subtraia o total de 11 para obter o primeiro dígito.

    Neste exemplo o resto da divisão de 194 por 11 é 7,
    então precisamos subtrair 11 - 7 para obter 4 como o primeiro dígito verificador.
    ###################################################################################


    2. Calcular o segundo dígito verificador.

    Para calcular o segundo dígito verificador vamos fazer um calculo similar ao do primeiro dígito,
    só que agora temos o primeiro digito verificador,
    então iremos multiplicar os primeiros dez números do CPF por 11, 10, 9, 8, 7, 6, 5, 4, 3 e 2
    respectivamente, e somar o resultado, exemplo:

    CPF 3 3 3 1 2 4 9 8 7 4

    Multiplicador 11 10 9 8 7 6 5 4 3 2

    Resultado 33 30 27 8 14 24 45 32 21 8

    A soma total de cada resultado é 242.

    Agora, divida o resultado total por 11,
    se o resto da divisão for menor que 2,
    então o segundo digito verificador é zero,
    caso contrário subtraia o total de 11 para obter o segundo dígito.

    Neste exemplo o resto da divisão de 242 por 11 é 0, então usamos zero como segundo dígito verificador.

    Então, para este exemplo de CPF: 333.124.987, temos o resultado 40 como dígito verificador.

    '''


    msgErroCpf = 'CPF Inválido'
    msgOKCpf = 'CPF Válido'

    '''
        CASOS DE TESTE

        * precisamos limpar os caracter especiais
            > limpar qualquer str e deixar apenas números
        * o cpf tem que ter 11 dígitos
            > testar com menos de 11 dígitos
            > testar com mais de 11 dígitos
        * verificar se o cpf é valido
            > testar os calculos para cada tipo de cpf
    '''

    def test_cpfLimparCaracter(self):
        '''Verifica se a função irá validar um cpf com formatação normal'''
        self.assertEqual(self.limparCaracter('830.360.756-15'),'83036075615')

    def test_cpfLimparCaracter2(self):
        '''Verifica se a função irá validar um cpf com formatação diferente'''
        self.assertEqual(self.limparCaracter("0'66.1s41.2#1 9-95"), self.msgErroCpf )

    def test_cpfVerificaQuantidade(self):
        '''Verificar se a quantidade do cpf é 11 dígitos'''
        self.assertEqual(self.limparCaracter(12345678952), '12345678952')

    def test_cpfVerificaQuantidadeErro1(self):
        '''Verifica quando a quantidade está acima de 11 dígitos'''
        self.assertEqual(self.limparCaracter(12345678952888), self.msgErroCpf)


    def test_cpfVerificaQuantidadeErro2(self):
        '''Verifica quando a quantidade está a baixo de 11 dígitos'''
        self.assertEqual(self.limparCaracter(1234567), self.msgErroCpf)


    def test_cpfVerificaOK(self):
        '''Verifica um cpf válido'''
        self.assertEqual(self.validaCpf('01069574651'), self.msgOKCpf)

    def test_cpfVerificaErro(self):
        '''Verifica um cpf inválido'''
        self.assertEqual(self.validaCpf('12345678952'), self.msgErroCpf)





    '''
    Exercício 1

    Crie uma aplicação para criptografar e descriptografar usando a Cifra de César.
    Esta é uma forma simples de criptografia, que consistem em mover as letras do alfabeto.
    Dado um número n, vamos mover as letras no alfabeto, por exemplo:

    Dado n = 3 e o texto “Rafael Guimaraes Sakurai”,
    vamos aumentar em 3 cada caractere, assim a versão criptografada ficará “Udidho Jzlpdudhv Vdnzudl”,
    e para descriptografar basta diminuir em 3 cada caractere.
    '''

    '''
        CASOS DE TESTE
        * validação ok
        * verificar com letras maiusculas
        * verificar com letras minnusculas
        * verificar com caracter especial
        *verificar com números
    '''

    def test_cifraCesarOK1(self):
        self.assertEqual(self.cifraCesar('a'),'d')

    def test_cifraCesarOK2(self):
        self.assertEqual(self.cifraCesar('z'),'c')

    def test_cifraCesarMaisculo(self):
        self.assertEqual(self.cifraCesar('B'), 'E')

    def test_cifraCesarMinusculo(self):
        self.assertEqual(self.cifraCesar('s'), 'v')

    def test_cifraCesarCaracter(self):
        self.assertEqual(self.cifraCesar('bah!'), 'edk!')

    def test_cifraCesarCaracter2(self):
        self.assertEqual(self.cifraCesar('João'), 'Mrdr')

    def test_cifraCesarComEspaco(self):
        self.assertEqual(self.cifraCesar('João da Silva'), 'Mrdr gd Vloyd')

    def test_cifraCesarComNumeros(self):
        self.assertEqual(self.cifraCesar('1a2g34B5'), '1d2j34E5')


if __name__ == '__main__':
    unittest.main()
