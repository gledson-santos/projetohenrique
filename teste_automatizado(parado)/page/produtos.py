import time
from selenium.webdriver.support.ui import Select


class Produtos(object):
    def __init__(self, driver):
        self.driver = driver

    def adicionarProduto(self, url, dadosProduto):
        self.driver.get(url+'/#/item/new')
        self.driver.execute_script("window.scrollTo(0, 600)")
        time.sleep(1)
        self.driver.find_element_by_xpath('//*[@id="box-new-product-search"]/div/p[3]/a').click()


        self.driver.find_element_by_xpath("//input[@ng-model='controller.currentItem.Name']").send_keys(dadosProduto['nomeProduto'])
        self.driver.find_element_by_xpath("//input[@ui-mask='9999']").send_keys(dadosProduto['codBalanca'])
        # SELECIONAR O TIPO DO PRODUTO
        tipo = Select(self.driver.find_element_by_xpath('//select[@ng-model="controller.currentItem.ItemTypeID"]'))
        tipo.select_by_visible_text(dadosProduto['tipoProduto'])
        #SELECIONAR A UNIDADE DO PRODUTO
        unidade = Select(self.driver.find_element_by_xpath("//select[contains(@ng-model,'controller.currentItem.ItemUnit')]"))
        unidade.select_by_visible_text(dadosProduto['unidadeProduto'])

        self.driver.find_element_by_xpath('//input[@ng-model="controller.currentItem.Cost"]').send_keys(dadosProduto['valorCusto'])
        self.driver.find_element_by_xpath('//input[@ng-model="controller.currentItem.SaleValue"]').send_keys(dadosProduto['valorVenda'])

        self.driver.execute_script("window.scrollTo(0, 600)")
        self.driver.find_element_by_xpath('//label[@ng-disabled="controller.hasSearchItem"]').click()
        try:
            return self.driver.find_element_by_class_name('modal-footer').text
        except:

            return 'Objeto Não encontrato'

